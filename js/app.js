
(function(){
  var thumbnails = document.querySelectorAll(".work_item");
  var closeBtn = document.querySelector("#closeBtn");
  var contactForm = document.querySelector("#contactForm");

  //open lightbox
  thumbnails.forEach(function(element, index){
    element.addEventListener('click', getInfo, false)
  });
  //close lightbox
  closeBtn.addEventListener('click', closeLightbox, false);
  //send Email
  contactForm.addEventListener('submit', sendEmail, false);

  //get image data from database
  function getInfo(){
    console.log("get image id " + this.id);
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function() {
      if(this.readyState == 4 && this.status == 200){
          var content = JSON.parse(this.responseText);

          var imgPath = content[0]["img_item"];
          var imgIntro = content[0]["img_desc"];
          document.querySelector("#lightboxImg").src = imgPath;
          document.querySelector("#imgDesc").innerHTML = imgIntro;
          document.querySelector("#lightbox").style.display = "block";
      }
    };
    xhttp.open("GET", "getInfo.php?getImage=" + this.id, true);
    xhttp.send();
  }

  //close lightbox
  function closeLightbox(){
    document.querySelector("#lightbox").style.display = "none";
  }

  //send Email
  function sendEmail(e){
    var name = document.querySelector("#name").value;
    var email = document.querySelector("#email").value;
    var phone = document.querySelector("#phone").value;
    var comments = document.querySelector("#comments").value;
    var sendMsg = new XMLHttpRequest();
    var url = "./scripts/mail.php";
    var params = "?username=" + name + "&emailAddr=" + email + "&phoneNumber=" + phone + "&comment=" + comments;
    url = url + params;
    sendMsg.open("POST", url, true);
    sendMsg.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    sendMsg.onreadystatechange = function() {
      if(this.readyState == 4 && this.status == 200){

          document.querySelector("#hiddenMsg").innerHTML = this.responseText;
          document.querySelector("#hiddenMsg").style.display = "block";
      }
    };
    sendMsg.send();
    e.preventDefault();
  }

  //foundation methods
  $(document).foundation();
})();
